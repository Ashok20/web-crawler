package myWebCrawler;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class Spider {

    private static final int MAX_PAGES_TO_SEARCH = 10;
    private Set<String> pagesVisited = new HashSet<String>();
    //private List<String> pagesToVisit = new LinkedList<String>();
    ArrayList<UrlDepth> ToVisit = new ArrayList<UrlDepth>();
    int nextUrlDepth = 0;

    public void search(String url) {

        String currentUrl;
        SpiderLeg leg = new SpiderLeg();

        do {

            if (this.ToVisit.isEmpty()) {
                currentUrl = url;
            } else {
                currentUrl = this.nextUrl();
            }
            leg.crawl(currentUrl); // Lots of stuff happening here. Look at the crawl method in
            // SpiderLeg
            boolean success = leg.searchForEmails();
            if (success) {
                System.out.println(String.format("**Success** Emails found at %s", currentUrl));
            }

            List UrlList = leg.getLinks();
            int noOfUrls = 0;
            noOfUrls = UrlList.size();
            
            if (nextUrlDepth < 2) {
                nextUrlDepth++;
                for (; noOfUrls > 0; noOfUrls--) {
                    ToVisit.add(ToVisit.size(), new UrlDepth(nextUrlDepth, UrlList.get(0).toString()));
                    //ToVisit.get(ToVisit.size()).setDepth(nextUrlDepth);
                    //ToVisit.get(ToVisit.size()).setUrl(UrlList.get(0).toString());
                    UrlList.remove(0);
                }
            }
            else
                System.out.println("\nNilukshi");
            
            
            System.out.println("\n**Done** Visited " + this.pagesVisited.size() + " web page(s)");
        }while (ToVisit.size() > 0);
    }

    /**
     * Returns the next URL to visit (in the order that they were found). We
     * also do a check to make sure this method doesn't return a URL that has
     * already been visited.
     *
     * @return
     */
    private String nextUrl() {
        String nextUrl;
        do {
            nextUrl = this.ToVisit.get(0).getUrl();
            nextUrlDepth = this.ToVisit.get(0).getDepth();
            this.ToVisit.remove(0);
        } while (this.pagesVisited.contains(nextUrl));
        this.pagesVisited.add(nextUrl);
        return nextUrl;
    }






  public void printt(){
      
      String[] pagevisitstring = pagesVisited.toString().split(",");
      
      for(String s:pagevisitstring){
      
          System.out.println(s);
      }
  }
    /**
     * Our main launching point for the Spider's functionality. Internally it
     * creates spider legs that make an HTTP request and parse the response (the
     * web page).
     *
     * @param url - The starting point of the spider
     * @param searchWord - The word or string that you are searching for
     */
    /*public void search(String url) {
        while (this.pagesVisited.size() < MAX_PAGES_TO_SEARCH) {
            String currentUrl;
            SpiderLeg leg = new SpiderLeg();
            if (this.pagesToVisit.isEmpty()) {
                currentUrl = url;
                this.pagesVisited.add(url);
            } else {
                currentUrl = this.nextUrl();
            }
            leg.crawl(currentUrl); // Lots of stuff happening here. Look at the crawl method in
            // SpiderLeg
            boolean success = leg.searchForEmails();
            if (success) {
                System.out.println(String.format("**Success** Emails found at %s", currentUrl));
                break;
            }
            this.pagesToVisit.addAll(leg.getLinks());
        }
        System.out.println("\n**Done** Visited " + this.pagesVisited.size() + " web page(s)");
    }*/
}
