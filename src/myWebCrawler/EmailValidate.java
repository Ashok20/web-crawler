package myWebCrawler;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidate {

    private Pattern pattern;
    private Matcher matcher;
    private List<String> emailList = new LinkedList<String>();

    private static final String EMAIL_PATTERN = "[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+";
    // = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
    // + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public EmailValidate() {
        pattern = Pattern.compile(EMAIL_PATTERN);
    }

    /**
     * Validate hex with regular expression
     *
     * @param hex hex for validation
     * @return true valid hex, false invalid hex
     */
    public boolean validate(final String hex) {

        matcher = pattern.matcher(hex);
        //String s = "*,mml** 33eest@gmail.com&gdg&^ _9tegfhjgst2@gmail.com(6343(sfgj& ";
        //Matcher m = Pattern.compile("[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+").matcher(s);

        //Store email address to List
        while (matcher.find()) {
            emailList.add(matcher.group());
        }

        for (int i = emailList.size()-1; i >= 0; i--) {
            System.out.println(emailList.get(i));
            System.out.println("*******************");
            //emailList.remove(0);
            //i++;
        }
        return matcher.matches();
    }
}
