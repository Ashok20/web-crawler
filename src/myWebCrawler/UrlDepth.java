package myWebCrawler;

public class UrlDepth {
    private int depth;
    private String url;
    
    
    public UrlDepth(int depth, String url){
        this.depth = depth;
        this.url = url;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getDepth() {
        return depth;
    }

    public String getUrl() {
        return url;
    }    
}
